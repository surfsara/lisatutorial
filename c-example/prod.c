// this program reads from standard input two numbers.
// these numbers are multiplied and put on standard output.

#include <stdio.h>
int main()
{
  double a,b,c;
  scanf("%lf %lf",&a,&b);  // read a and b from stdin
  c = a*b;                 // compute the product
  printf("%lf\n",c);       // print the product
  return 0;
}
